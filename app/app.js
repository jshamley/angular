var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var angular2_1 = require('angular2/angular2');
var myService_1 = require('./myService');
var SubComponent = (function () {
    function SubComponent(MyService) {
        this.MyService = MyService;
        console.log(MyService.myInfo);
    }
    SubComponent.prototype.onEnter = function (myBox) {
        this.MyService.addInfo(myBox);
    };
    SubComponent = __decorate([
        angular2_1.Component({
            selector: 'sub-comp'
        }),
        angular2_1.View({
            directive: [angular2_1.NgFor],
            template: "\n\t<div *ng-for=\"#info of MyService.myInfo\">\n\t<h2><em>{{ info }}</em></h2>\n\t</div>\n\t<div>\n\t\t<input #my-box (keyup.enter)=\"onEnter(myBox.value)\">\n\t</div>\n\t"
        }), 
        __metadata('design:paramtypes', [myService_1.myService])
    ], SubComponent);
    return SubComponent;
})();
var TSComponent = (function () {
    function TSComponent() {
        this.myFramework = "Angular2";
    }
    TSComponent = __decorate([
        angular2_1.Component({
            selector: 'ts-comp'
        }),
        angular2_1.View({
            template: "\n\t\t<h2>I am learning </h2>\n\t\t<sub-comp></sub-comp>\n\t\t",
            directives: [SubComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], TSComponent);
    return TSComponent;
})();
angular2_1.bootstrap(TSComponent, [myService_1.myService]);
//# sourceMappingURL=app.js.map