var myService = (function () {
    function myService() {
        this.myInfo = ['Angular2', 'jQuery', 'JavaScript'];
    }
    myService.prototype.addInfo = function (value) {
        this.myInfo.push(value);
    };
    return myService;
})();
exports.myService = myService;
//# sourceMappingURL=myService.js.map